
"use client"


import {useEffect, useState} from "react";
import {Autocomplete, Box, Button, Grid, TextField} from "@mui/material";





export default function Home() {
  const [data, setData] = useState(null)
  const [isLoading, setLoading] = useState(true)
  var action = "";
  var actionId = "";
  useEffect(() => {
    fetch('/api/todo')
        .then((res) => res.json())
        .then((data) => {
          setData(data)
          setLoading(false)
        })
  }, [])

  if (isLoading) return <p>Loading...</p>
  if (!data) return <p>No profile data</p>
  function Remove(data,id){
    var temp;
    for(var i=0;i<data.length;i++){
      if(data[i].id == id){
        temp = data[i];
        break;
      }
    }


    const filtered = data.filter(obj => {
      // 👇️ returns truthy for all elements that
      // don't have an id equal to 2
      return obj.id !== id;
    });
    setData(filtered);

    //call api
          
    fetch('/api/todo/'+id,{
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(temp),
    })
    .then((res) => res.json())
    .then((data) => {
      console.log('>>>>>>>>>data: '+JSON.stringify(data));
    });

    alert("Success!");
    
  }
  function Edit(data,newTodo){
      if(newTodo == ""){
        alert("Todo cannot be empty");
        return;
      }
      //check duplicate
      var index = 0;
      for(var i=0;i<data.length;i++){
        if(data[i].id != actionId && data[i].todo == newTodo){
          alert(newTodo+" already exist.");
          return;
        }else if(data[i].id == actionId){
          index = i;
        }
      }
      data[index].todo = newTodo;
      //call api
      fetch('/api/todo',{
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data[index]),
      })
      .then((res) => res.json())
      .then((data) => {
        console.log('>>>>>>>>>data: '+JSON.stringify(data));
      });
      alert("Success!");
      action = "";
      
  }
  function AddNew(data,newTodo){
    if(newTodo == ""){
      alert("Todo cannot be empty");
      return;
    }
    //check duplicate
    for(var i=0;i<data.length;i++){
      if(data[i].todo == newTodo){
        alert(newTodo+" already exist.");
        return;
      }
    }
    const newId = data[data.length]+"";
    const newTodoItem = {id:newId,todo:newTodo,isCompleted:false,createdAt:"2023-08-19 00:00:00"};
    data.push(newTodoItem);
    setData(data);

    //call api
          
    fetch('/api/todo',{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newTodoItem),
    })
    .then((res) => res.json())
    .then((data) => {
      console.log('>>>>>>>>>data: '+JSON.stringify(data));
    });
    alert("Success!");
  }
  function MarkTodoList(data,id,isCompleted){
    for(var i=0; i<data.length; i++){
      if(data[i].id == id){
        data[i].isCompleted = isCompleted;
        break;
      }
    }
  }
  return (
      <div className="flex flex-col min-h-screen mx-auto max-w-2xl px-4 pt-8 pb-16">
          <div className="flex-grow">

              <main className="my-0 py-16">
          <Autocomplete
              disablePortal
              id="combo-box-demo"
              noOptionsText="No result. Create a new one instead!"
              options={data}
              getOptionLabel={(option) => option.todo}
              sx={{ width: 700 }}
              renderOption={(props, option) => (
                  <Grid component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
                      {
                        option.isCompleted ?
                        <span style={{textDecoration: 'line-through'}}>{option.todo}</span> :
                        <span>{option.todo}</span>
                      }
                       
                       &nbsp;&nbsp;
                        <Button
                            variant="contained"
                            onClick={()=>{
                              action = "remove";
                              Remove(data,option.id);
                            }}
                            children = "remove"
                        >

                        </Button>&nbsp;&nbsp;
                      <Button
                          variant="contained"
                          onClick={() => {
                            action = "edit";  
                            actionId = option.id;
                        }}
                          children = "edit"
                      >

                      </Button>&nbsp;&nbsp;
                      <Button
                          variant="contained"
                          children = {
                            option.isCompleted ? "Mark as Incomplete" : "Mark as Complete"
                          }
                          onClick={() => {
                            MarkTodoList(data,option.id,option.isCompleted ? false : true);
                          }}
                      >

                      </Button>
                  </Grid >
              )}
              renderInput={(params) => (
                  <TextField
                      {...params}
                      label="Choose a todo list"
                      inputProps={{
                        ...params.inputProps,
                        autoComplete: 'new-password', // disable autocomplete and autofill
                      }}
                      onKeyDown={(e)=>{
                        if(e.keyCode == 13){
                          console.log('value', e.target.value);
                          if(action == "edit"){
                            Edit(data,e.target.value);
                          }else{
                            AddNew(data,e.target.value);
                          }
                       }
                      }}
                  />
              )}
          />
              </main>
          </div>

      </div>
  )
}
