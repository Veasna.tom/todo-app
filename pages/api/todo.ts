import { NextApiRequest, NextApiResponse } from 'next'

var top100Films = [
    { id:"1",todo:"A1",isCompleted:false,createdAt:"2023-08-19 00:00:00" },
    { id:"2",todo:"B1",isCompleted:false,createdAt:"2023-08-19 00:00:00" },
    { id:"3",todo:"C1",isCompleted:false,createdAt:"2023-08-19 00:00:00" },
    { id:"4",todo:"D1",isCompleted:false,createdAt:"2023-08-19 00:00:00" },
    { id:"5",todo:"E1",isCompleted:false,createdAt:"2023-08-19 00:00:00" },
    { id:"6",todo:"F1",isCompleted:false,createdAt:"2023-08-19 00:00:00" },
    { id:"7",todo:"G1",isCompleted:false,createdAt:"2023-08-19 00:00:00" },
    { id:"8",todo:"H1",isCompleted:false,createdAt:"2023-08-19 00:00:00" },
    { id:"9",todo:"I1",isCompleted:false,createdAt:"2023-08-19 00:00:00" },
    { id:"10",todo:"J1",isCompleted:false,createdAt:"2023-08-19 00:00:00" }
];
export default function handler(req: NextApiRequest, res: NextApiResponse) {
    if(req.method == 'GET'){
        res.status(200).json(top100Films)
    }else if(req.method == 'DELETE'){
        console.log('>>>>>>>>>delete_body: '+JSON.stringify(req.body));
        const body = JSON.parse(JSON.stringify(req.body));
        top100Films = top100Films.filter(obj => {
            // 👇️ returns truthy for all elements that
            // don't have an id equal to 2
            return obj.id !== body.id;
          });
        res.status(200).json({result:"Success!"});
    }else if(req.method == 'PUT'){
        console.log('>>>>>>>>>put_body: '+JSON.stringify(req.body));
        const body = JSON.parse(JSON.stringify(req.body));
        for(var i=0;i<top100Films.length;i++){
            if(top100Films[i].id == body.id){
                top100Films[i] = body;
                res.status(200).json({result:"Success!"});
            }
        }
        res.status(404).json({result:"Data Not Found"});
    }else if(req.method == 'POST'){
        console.log('>>>>>>>>>post_body: '+JSON.stringify(req.body));
        const body = JSON.parse(JSON.stringify(req.body));
        top100Films.push(body);
        res.status(200).json({result:"Success!"});
    }
}